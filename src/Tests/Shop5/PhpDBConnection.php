<?php

declare(strict_types=1);

namespace Systemcheck\Tests\Shop5;

use PDO;
use Systemcheck\Platform\DBServerInfo;
use Systemcheck\Platform\PDOConnection;
use Systemcheck\Tests\ProgramTest;

/**
 * Class PhpDBConnection
 * @package Systemcheck\Tests\Shop5
 */
class PhpDBConnection extends ProgramTest
{
    protected string $name = 'Datenbank-Unterstützung';

    protected string $requiredState = 'MySQL, MariaDB';

    protected string $description = 'Für JTL-Shop wird eine MySQL oder MariaDB Datenbank benötigt.';

    /** @var bool */
    private bool $requireAuth = false;

    /**
     * @param PDO|null $pdoDB
     * @return bool
     */
    private function handleAuthenticated(?PDO $pdoDB): bool
    {
        if ($pdoDB === null) {
            $this->requireAuth  = true;
            $this->currentState = 'Anmeldung fehlgeschlagen';

            return false;
        }

        $version            = new DBServerInfo($pdoDB);
        $this->requireAuth  = false;
        $this->currentState = $version->getServer();

        return $version->isSupportedServer();
    }

    /**
     * @return bool
     */
    private function handleNotAuthenticated(): bool
    {
        $this->requireAuth  = true;
        $this->currentState = 'Login erforderlich';

        return false;
    }

    /**
     * @return bool
     */
    private function handleNotSupported(): bool
    {
        $this->requireAuth  = false;
        $this->currentState = 'nicht unterstützt';

        return false;
    }

    /**
     * @inheritdoc
     */
    public function execute(): bool
    {
        $pdoTest = new PhpPdoMysqlSupport();
        if (!$pdoTest->execute()) {
            return $this->handleNotSupported();
        }
        $pdoCon = PDOConnection::getInstance();
        $pdoDB  = $pdoCon->getConnection();
        if ($pdoDB !== null) {
            return $this->handleAuthenticated($pdoDB);
        }
        if ((int)($_POST['dbAuth'] ?? 0) === 1) {
            $pdoDB = $pdoCon->getConnection(
                PDOConnection::createAuth(
                    $_POST['dbHost'] ?? '',
                    $_POST['dbSocket'] ?? '',
                    $_POST['dbUser'] ?? '',
                    $_POST['dbPassword'] ?? ''
                )
            );

            return $this->handleAuthenticated($pdoDB);
        }
        if (\defined('DB_USER') && \defined('DB_PASS') && \defined('DB_HOST')) {
            $pdoDB = $pdoCon->getConnection(
                PDOConnection::createAuth(
                    \DB_HOST,
                    \defined('DB_SOCKET') ? \DB_SOCKET : '',
                    \DB_USER,
                    \DB_PASS
                )
            );

            return $this->handleAuthenticated($pdoDB);
        }

        return $this->handleNotAuthenticated();
    }

    /**
     * @inheritdoc
     */
    public function getDescription(): string
    {
        $default = parent::getDescription();
        if ($this->requireAuth) {
            $default .= '<form method="post" class="db_check_form">'
                . '<div class="row mb-3">'
                . '<input type="hidden" name="dbAuth" value="1" />'
                . '<div class="col cold-md6">'
                . '<label for="dbHost" class="form-label">Datenbank Host</label>'
                . '<input id="dbHost" type="text" class="form-control mb-3" name="dbHost" value="localhost" />'
                . '<label for="dbSocket" class="form-label">Socket (optional)</label>'
                . '<input id="dbSocket" type="text" class="form-control" name="dbSocket" />'
                . '</div><div class="col cold-md6">'
                . '<label for="dbUser" class="form-label">Benutzername</label>'
                . '<input id="dbUser" type="text" class="form-control mb-3" name="dbUser" />'
                . '<label for="dbPassword" class="form-label">Passwort</label>'
                . '<input id="dbPassword" type="password" class="form-control" name="dbPassword" />'
                . '</div></div>'
                . '<button type="submit" class="btn btn-primary">Testen</button>'
                . '</form>';
        }

        return $default;
    }
}
